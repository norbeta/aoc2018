import numpy
import re
from tabulate import tabulate
from PIL import Image

#First create the grid
a = numpy.zeros( (1000,1000), dtype=int )

f = open('3.file', 'r')
l = f.readline()

pic = Image.new('RGB', [1000, 1000], 'red')

while l:
    line = re.split(' |:|,|x|\n', l)
    id = line[0]
    cx = line[2]
    cy = line[3]
    sx = line[5]
    sy = line[6]

    xlimit = int(cx) + int(sx)
    ylimit = int(cy) + int(sy)

    for x in range(int(cx), int(xlimit)):
        for y in range(int(cy), int(ylimit)):
#            print(str(x) +'x' +str(y) +' limits: ' +str(xlimit) +'x' +str(ylimit))
            a[x][y] += 1
            xy = (x, y)
            if id == "#894":
                pic.putpixel(xy, (0,255,0))
            else:
                pic.putpixel(xy, (255,255,255))
    l = f.readline()

f.close()

pic.save('test2.png')

#print(numpy.count_nonzero(a >= 2))

#table = tabulate(a)
#o = open('3.out','w')
#o.write(table)
#o.close()

