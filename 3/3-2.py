import numpy
import re
from tabulate import tabulate

#First create the grid
a = numpy.zeros( (1000,1000), dtype=int )

f = open('3.file', 'r')
l = f.readline()
# First fill the grid
while l:
    line = re.split(' |:|,|x|\n', l)
    id = line[0]
    cx = line[2]
    cy = line[3]
    sx = line[5]
    sy = line[6]

    xlimit = int(cx) + int(sx)
    ylimit = int(cy) + int(sy)

    for x in range(int(cx), int(xlimit)):
        for y in range(int(cy), int(ylimit)):
            a[x][y] += 1
    l = f.readline()

f.close()

f = open('3.file', 'r')
l = f.readline()
# For verification after grid is filled. 
while l:
    b = 0
    line = re.split(' |:|,|x|\n', l)
    id = line[0]
    cx = line[2]
    cy = line[3]
    sx = line[5]
    sy = line[6]

    xlimit = int(cx) + int(sx)
    ylimit = int(cy) + int(sy)
    size = int(sx) * int(sy)
    
    for x in range(int(cx), int(xlimit)):
        for y in range(int(cy), int(ylimit)):
            if a[x][y] == 1:
                b += 1
#   print('size: ' +str(size) +' b: ' +str(b))
    if b == size:
        print('ID: ' +str(id))
    l = f.readline()

f.close()
